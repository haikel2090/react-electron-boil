const electron = require('electron');
const path = require('path');
const { app, BrowserWindow } = electron;

let mainWindow;
let isProduction = true;

function createWindow () {
  mainWindow = new BrowserWindow({
    webPreferences: { nodeIntegration: true, preload: path.join(__dirname, "preload.js") }
  });

  mainWindow.maximize();

  isProduction ? mainWindow.loadFile(__dirname + '/build/index.html')
    : mainWindow.loadURL('http://localhost:3000');

  mainWindow.webContents.openDevTools();

  mainWindow.on('closed', function () {
    mainWindow = null
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})
